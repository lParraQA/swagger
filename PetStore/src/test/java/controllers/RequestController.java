package controllers;


import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;

import static io.restassured.RestAssured.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class RequestController {
    RequestSpecification rs=given();

    public RequestController(){
    }

    public void setHeaders(String[]... headers){

        rs.headers(headers[0][0], headers[0][1],headers[1][0], headers[1][1]);
        rs.baseUri("https://petstore.swagger.io/v2/");
    }

    public Response sendRequest(String method, String resource,Object... o){



        System.out.println("Se va a enviar la petición");

        switch(method){
            case "GET":
                System.out.println(resource);
                return rs.get(resource);

            case "POST":
                return rs.body(o[0]).when().post(resource);

            default:
                return null;
        }

    }


}
