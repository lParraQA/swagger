package definitions;

import com.google.inject.internal.cglib.reflect.$FastMethod;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import implementation.Implementation;
import net.thucydides.core.annotations.Steps;

public class CommonSteps {
    @Steps(shared = true)
    Implementation imp;

    @Given("^I set base path for \"([^\"]*)\"$")
    public void iSetBasePathFor(String baseUri) throws Throwable {
        imp.setBaseUri(baseUri);
    }


    @And("^I set header \"([^\"]*)\" to \"([^\"]*)\"$")
    public void iSetHeaderTo(String arg0, String arg1) throws Throwable {
        imp.setHeaders(arg0,arg1);
    }

    @When("^I set operation \"([^\"]*)\"$")
    public void iSetOperation(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }


    @And("^I execute the request$")
    public void iExecuteTheRequest() {
    }

    @Then("^Response status code is \"([^\"]*)\"$")
    public void responseStatusCodeIs(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I set resource \"([^\"]*)\"$")
    public void iSetResource(String resource) throws Throwable {
        imp.setResource(resource);
    }

    @Then("^I hope a (\\d+) status to the \"([^\"]*)\" request$")
    public void iHopeAStatusToTheRequest(int code, String method) throws Throwable {

    }

    @When("^I send the \"([^\"]*)\" request$")
    public void iSendTheRequest(String method) throws Throwable {
        imp.sendRequest(method);
    }

    @Then("^I hope a (\\d+) status$")
    public void iHopeAStatus(int status) throws Throwable {
        imp.isStatusCode(status);
    }


}


