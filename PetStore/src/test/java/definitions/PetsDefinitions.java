package definitions;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import implementation.Implementation;
import implementation.PetImplementation;
import net.thucydides.core.annotations.Steps;

public class PetsDefinitions {
    @Steps(shared = true)
    Implementation imp;
    @And("^I set body with a new pet$")
    public void iSetBodyWithANewPet() {

    }

    @And("^I set body with a new pet and id \"([^\"]*)\"$")
    public void iSetBodyWithANewPetAndId(String id) throws Throwable {
        imp.setBody(PetImplementation.setRandomPet(id));
    }

    @And("^A new pet is created in the system with id \"([^\"]*)\"$")
    public void aNewPetIsCreatedInTheSystemWithId(String id) throws Throwable {
        imp.setResource("/pet");
        imp.setHeaders( "Content-type","application/json");
        imp.setBody(PetImplementation.setRandomPet(id));
        imp.sendRequest("POST");
        imp.isStatusCode(200);
    }

    @And("^A a pet with id \"([^\"]*)\" is deleted from the system$")
    public void aAPetWithIdIsDeletedFromTheSystem(String id) throws Throwable {
        imp.setResource("/pet"+"/"+id);
        imp.setHeaders( "Accept","application/json");
        imp.sendRequest("DEL");
        imp.isStatusCode(200);
    }
}
