package implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.mapper.ObjectMapperDeserializationContext;
import io.restassured.mapper.ObjectMapperSerializationContext;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.exceptions.SerenityManagedException;
import org.junit.Assert;
import representations.Pet;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.proxy;

public class Implementation {
    private RequestSpecification rs = given();
    private String resource;
    private Response response;

    public void setHeaders(String headerName,String headerValue){

        rs.header(headerName, headerValue);
    }

    public void setBody (Object o){
        ObjectMapper mapper=new ObjectMapper();

        try {
            System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(o));
        } catch (JsonProcessingException e) {
            System.err.println("JsonProcessingException:   Ha habido un error con la serialización del cuerpo y no se puede mostrar por pantalla");
        }
        try {
            rs.body(o);
        }catch(Exception e){
            try {
                rs.body(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(o));
            } catch (JsonProcessingException jpe) {
                System.out.println("Imposible serializar");
            }
        }
    }

    public void setBaseUri (String baseUri) {
        rs.baseUri(baseUri);
    }

    public void setResource(String resource){
        this.resource=resource;
    }

    public void sendRequest (String operation) throws Exception {
        switch (operation) {
            case ("GET"):
                response = rs.get(this.resource);
                break;
            case("PUT"):
                response = rs.put(this.resource);
                break;
            case ("POST"):
                response = rs.post(this.resource);
                break;
            case("DEL"):
            case ("DELETE"):
                response = rs.delete(this.resource);
                break;
            default:
                System.out.println("Error");
        }
    }
    public void isStatusCode(int statusCode) throws Exception {
        try{
            System.out.println("Comparación");
            Assert.assertEquals(response.statusCode(),statusCode);
        } catch(AssertionError e){
            throw new Exception("Respuesta a la petición incorrecta...     Code: "+response.statusCode());
        }
        System.out.println("Code: "+statusCode);
    }


}