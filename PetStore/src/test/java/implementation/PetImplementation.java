package implementation;

import representations.Pet;
import utils.UtilMethods;

public class PetImplementation {
    public static Pet setRandomPet() {
        short id= (short) UtilMethods.getRandomInteger(1000);
        String name=UtilMethods.getRandomString(15,5);
        int catId=UtilMethods.getRandomInteger();
        String catName=UtilMethods.getRandomString(3,2)+"CAT";

        return new Pet(id,name,catId,catName,"available");
    }
    public static Pet setRandomPet(String idString) {
        short id= Short.parseShort(idString);
        String name=UtilMethods.getRandomString(15,5);
        int catId=UtilMethods.getRandomInteger();
        String catName=UtilMethods.getRandomString(3,2)+"CAT";

        return new Pet(id,name,catId,catName,"available");
    }
}
