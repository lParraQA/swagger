package representations;

import javax.naming.event.ObjectChangeListener;

enum Status
{
    available, pending, sold
}

class Tag{
    int id;
    String name;
}

class Category{
    private int id;
    private String name;

    void setCat(String name,int id){
        this.name=name;
        this.id=id;
    }
}

public class Pet {

    private int id;
    private String name;
    private Status status;
    private Category category=new Category();
    private Tag tags[];

    public Pet(int id, String name, int idCat,String nameCat, String status){
        this.id=id;
        this.name=name;
        this.category.setCat(nameCat,idCat);
       /* short i=0;
        for(Tag tag : tags){
            this.tags[i]=tag;
            i++;
        }*/
        this.status=Status.valueOf(status);
    }

    public void setStatus(String s){
        status=Status.valueOf(s);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



}
