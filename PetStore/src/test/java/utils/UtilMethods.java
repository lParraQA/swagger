package utils;

public class UtilMethods {

    public static int getRandomInteger(){
        return (int)(Math.random()*(10));
    }
    public static int getRandomInteger(int maxNumber){
        return (int)(Math.random()*(maxNumber));
    }
    public static int getRandomInteger(int maxNumber, int startNumber){
        return (int)(Math.random()*(maxNumber-startNumber)+startNumber);
    }

    public static String getRandomString (){
        String word="";
        short maxSize= (short) UtilMethods.getRandomInteger(18,2);
        for(short i=0; i<maxSize;i++){
            word+=(char)UtilMethods.getRandomInteger(26,97);
        }
        return word;
    }
    public static String getRandomString (int maxSize,int minSize){
        String word="";
        short size= (short) UtilMethods.getRandomInteger(maxSize-minSize,minSize);
        for(short i=0; i<size;i++){
            int caracterCode=UtilMethods.getRandomInteger(122,97);
            word+=(char)caracterCode;
        }
        return word;
    }
}
