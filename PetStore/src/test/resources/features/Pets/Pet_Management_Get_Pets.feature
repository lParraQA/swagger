Feature: Look for a pet

  Scenario Outline: Look for a pet successfully
    Given I set base path for "https://petstore.swagger.io/v2"
    And A new pet is created in the system with id "<id>"
    And I set resource "/pet/<id>"
    And I set header "Accept" to "application/json"
    When I send the "GET" request
    Then I hope a 200 status
    Examples:
      | id |
      | 1 |
      | 7 |
      | 8 |
      | 3 |
      | 5 |

  Scenario Outline: Look for a pet unsuccessfully because the pet currently doesn´t exists
    Given I set base path for "https://petstore.swagger.io/v2"
    And A a pet with id "<id>" is deleted from the system
    And I set resource "/pet/<id>"
    And I set header "Accept" to "application/json"
    When I send the "GET" request
    Then I hope a 404 status
    Examples:
      | id |
      | 1 |
      | 7 |
      | 8 |
      | 3 |
      | 5 |

  Scenario Outline: Look for for a pet unsuccessfully because of unsupported format
    And I set base path for "https://petstore.swagger.io/v2"
    And A new pet is created in the system with id "<id>"
    And I set resource "/pet/<id>"
    And I set header "Accept" to "text/html"
    When I send the "GET" request
    Then I hope a 406 status
    Examples:
      | id |
      | 1 |
      | 7 |
      | 8 |
      | 3 |
      | 5 |