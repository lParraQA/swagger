Feature: Post a Pet

  Scenario Outline: Substituting a pet successfully
    Given I set base path for "https://petstore.swagger.io/v2"
    And A new pet is created in the system with id "<id>"
    And I set resource "/pet"
    And I set header "Content-type" to "application/json"
    And I set body with a new pet and id "<id>"
    When I send the "POST" request
    Then I hope a 200 status
    Examples:
      | id |
      | 3237 |
      | 3242 |
      | 342 |
      | 823 |


  Scenario Outline: Creating a pet successfully
    Given I set base path for "https://petstore.swagger.io/v2"
    And A a pet with id "<id>" is deleted from the system
    And I set resource "/pet"
    And I set header "Content-type" to "application/json"
    And I set body with a new pet and id "<id>"
    When I send the "POST" request
    Then I hope a 200 status
    Examples:
      | id |
      | 3237 |
      | 3242 |
      | 342 |
      | 823 |

  Scenario: Post a pet unsuccessfully because of unssoported format
    Given I set base path for "https://petstore.swagger.io/v2"
    And I set resource "/pet"
    And I set header "Content-type" to "text/html"
    And I set body with a new pet
    When I send the "POST" request
    Then I hope a 415 status