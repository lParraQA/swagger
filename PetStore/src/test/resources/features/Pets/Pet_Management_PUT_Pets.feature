Feature: Put a Pet
  Scenario Outline: Updating a pet successfully
    Given I set base path for "https://petstore.swagger.io/v2"
    And A new pet is created in the system with id "<id>"
    And I set resource "/pet"
    And I set header "Content-type" to "application/json"
    And I set body with a new pet and id "<id>"
    When I send the "PUT" request
    Then I hope a 200 status
    Examples:
    | id |
    | 3237 |
    | 3242 |
    | 342 |
    | 823 |

  Scenario Outline: Updating a pet that doesn´t exists unsuccessfully
    Given I set base path for "https://petstore.swagger.io/v2"
    And A a pet with id "<id>" is deleted from the system
    And I set resource "/pet"
    And I set header "Content-type" to "application/json"
    And I set body with a new pet and id "<id>"
    When I send the "PUT" request
    Then I hope a 404 status
    Examples:
      | id |
      | 3237 |
      | 3242 |
      | 342 |
      | 823 |