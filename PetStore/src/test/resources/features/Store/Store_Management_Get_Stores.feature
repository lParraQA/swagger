Feature: Inventory

  Scenario: look for the inventory successfully
    Given I set base path for "https://petstore.swagger.io/v2"
    And I set resource "/store/inventory"
    And I set header "Accept" to "application/json"
    When I send the "GET" request
    Then I hope a 200 status

  Scenario: look for the inventory unsuccessfully because of unsopported format
    Given I set base path for "https://petstore.swagger.io/v2"
    And I set resource "/store/inventory"
    And I set header "Accept" to "text/html"
    When I send the "GET" request
    Then I hope a 406 status